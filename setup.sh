#!/bin/bash

# Copyright (C) 2013, 2014, 2015, 2016 Digital Freedom Foundation
#  This file is part of GNUKhata:A modular,robust and Free Accounting System.

#  GNUKhata is Free Software; you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.

#  GNUKhata is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#  You should have received a copy of the GNU Affero General Public
#  License along with GNUKhata (COPYING); if not, write to the
#  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA  02110-1301  USA59 Temple Place, Suite 330,


# Contributor:
# Abhijith Balan <abhijth@dff.org.in>

echo "GNUKhata is being installed..."
echo "Installing Dependencies..."
sudo apt-get install python-virtualenv postgresql postgresql-client python-dev libpq-dev git nginx
echo "Creating A Directory..."
mkdir GNUKhata
cd GNUKhata
echo "Downloading files from GitLab..."
git clone "https://gitlab.com/gnukhata/gkwebapp.git"
git clone "https://gitlab.com/gnukhata/gkcore.git"
echo "Setting Up A Virtual Environment to Install Python Packages..."
virtualenv gkenv
source gkenv/bin/activate
pip install pyramid pyramid_debugtoolbar supervisor psycopg2 odslib sqlalchemy waitress wsgicors requests monthdelta pyjwt pillow babel pycrypto openpyxl
echo "Setting Up GNUKhata...."
cd gkwebap
python setup.py develop
cd ../gkcore
python setup.py develop
deactivate
echo "Setting Up Database..."
chmod 755 gkutil.sh
./gkutil.sh
sudo su gkadmin
source ../gkenv/bin/activate
python initdb.py
exit
cd ..
echo "Almost there..."
sudo cp nginx.conf /etc/nginx/
sudo cp gnukhata.conf /etc/nginx/conf.d/
chmod 775 startscript.sh
./startscript.sh
sudo cp gnukhata /usr/bin
sudo chmod 755 /usr/bin/gnukhata
sudo cp gnukhata.desktop /usr/share/applications
sudo mkdir /usr/share/gnukhata
sudo cp gnukhata.svg /usr/share/gnukhata
sudo chmod 755 /usr/share/applications/gnukhata.desktop
sudo chmod 755 /usr/share/gnukhata/gnukhata.svg
echo "Done..."
echo 'GNUKhata is your freedom in accounting'
